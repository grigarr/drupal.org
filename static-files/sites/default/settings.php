<?php

/**
 * Put private settings, like $databases, in settings.local.php.
 *
 * Drupal.org-wide settings, like Bakery, go in /var/www/settings.common.php.
 */

$update_free_access = FALSE;
$drupal_hash_salt = '';

ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 100);
ini_set('session.gc_maxlifetime', 200000);
ini_set('session.cookie_lifetime', 2000000);

if ( (isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on')
  || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
  || (isset($_SERVER['HTTP_HTTPS']) && $_SERVER['HTTP_HTTPS'] == 'on')
) {
  $_SERVER['HTTPS'] = 'on';
}


$conf += array(
  'queue_class_versioncontrol_repomgr' => 'BeanstalkdQueue',
  'beanstalk_queue_versioncontrol_repomgr' => array(
    'reserve_timeout' => NULL,
  ),
  'queue_class_versioncontrol_git_repo_parsing' => 'BeanstalkdQueue',
  'beanstalk_queue_versioncontrol_git_repo_parsing' => array(
    'reserve_timeout' => NULL,
  ),
  'queue_class_versioncontrol_reposync' => 'BeanstalkdQueue',
  'beanstalk_queue_versioncontrol_reposync' => array(
    'reserve_timeout' => NULL,
  ),
  'versioncontrol_release_label_version_mapper' => 'drupalorg_git',
  'waiting_queue_process_lifetime_versioncontrol_repomgr' => 900,
  'waiting_queue_process_lifetime_versioncontrol_git_repo_parsing' => 900,
  'waiting_queue_process_lifetime_versioncontrol_git_repo_activity_stream' => 900,
  // This var defaults to FALSE in the important places, but is re-set here for
  // safety. Turning this var true will expose password MD5s, so it MUST BE
  // FALSE FOR ANY PUBLIC-FACING INSTANCE. Override it in settings.local.php
  // if appropriate.
  'drupalorg_is_internal_varnish_instance' => FALSE,
);

$conf['404_fast_paths_exclude'] = '/\/(?:styles)\//';
$conf['404_fast_paths'] = '/\.(?:txt|png|gif|jpe?g|css|js|ico|swf|flv|cgi|bat|pl|dll|exe|asp)$/i';
$conf['404_fast_html'] = '<html xmlns="http://www.w3.org/1999/xhtml"><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL "@path" was not found on this server.</p></body></html>';

$conf['tracker_batch_size'] = 1000;
$conf['upload_update_batch_size'] = 1000;

// Disable poormanscron.
$conf['cron_safe_threshold'] = 0;

// We don't use link theming.
$conf['theme_link'] = FALSE;

// Turn off node searching, turn on Drupalorg (IRC nickname) searching, and set
// default search to Solr.
$conf['search_active_modules'] = array('user', 'drupalorg', 'apachesolr_search');
$conf['search_default_module'] = 'apachesolr_search';

$conf['apachesolr_site_hash'] = 'e7ikco';

// Release packaging.
$conf['project_release_security_update_tid'] = 100;
$conf['project_release_packager_plugin_project_distribution'] = 'drupalorg_distro';
$conf['project_release_packager_plugin'] = 'drupalorg';

// Project.
$conf['project_sandbox_numeric_shortname'] = TRUE;
$conf['project_release_sandbox_allow_release'] = FALSE;
$conf['project_allow_machinename_update'] = FALSE;
$conf['project_require_lowercase_machinename'] = TRUE;
$conf['project_usage_updates_regex'] = '@\[([^\]]+)\] "?(?:GET|HEAD) https?://updates.drupal.org/(?:80C301/updates.drupal.org/)?release-history/([^/]+)/([^\?]+)\?([^ ]+) @';

// drupalorg custom lists module
$conf['lists_security_mail'] = 'security-news@drupal.org';
$conf['lists_security_url'] = 'https://lists.drupal.org/mailman/admin/security-news';
$conf['lists_maintainer_mail'] = 'maintainers-news@drupal.org';
$conf['lists_maintainer_url'] = 'https://lists.drupal.org/mailman/admin/maintainers-news';
$conf['lists_forum_tids'] = array(
  42 => 'drupal',
  44 => 'security',
  1852 => 'security',
  1856 => 'security',
  118 => 'maintainer',
);

// Drupal.org settings.
$conf['drupalorg_crosssite_lock_permissions'] = TRUE;
$conf['logintoboggan_pre_auth_role'] = 39;
$conf['drupalorg_crosssite_trusted_role'] = 36;
$conf['drupalorg_crosssite_community_role'] = 7;
$conf['drupalorg_headless_form_paths'] = array(
  'user/register',
  'user/password',
  'user/login',
  'welcome',
);
// Social link patterns.
$conf['drupalorg_social_link_patterns'] = array(
  '%^https?://((drupal|meta)\.)?stack(exchange|overflow)\.com/users/[0-9]+%' => 'Drupal Answers',
  '%^https?://([a-z-]+\.)?(facebook|fb)\.[a-z]+/(#!/)?([^/]+|(pages|people)/[^/]+(/[^/]+)?/[0-9]+|groups/[^/]+)/?(\\?.*)?$%' => 'Facebook',
  '%^https?://(www\.)?github\.com/[^/]+/?$%' => 'GitHub',
  '%^https?://((plus|www)\.)?google\.com/(u/[0-9]/)?(b/)?((\\+|s/)[^/]+|[0-9]+)%' => 'Google',
  '%^https?://profiles\.google\.com/%' => 'Google',
  '%^https?://(www\.)?google\.(com|de)/profiles/%' => 'Google',
  '%^https?://(gratipay|(www\.)?gittip)\.com/[^/]+/?$%' => 'Gratipay',
  '%^https?://([a-z]+\.)?linkedin\.com/((in|company|companies)/[^/]+(/[a-z]+)?|pub/[^/]+/[^/]+/[^/]+/[^/]+)/?$%' => 'LinkedIn',
  '%^https?://(www\.)?twitter\.com/(#!/)?[^/]+/?$%' => 'Twitter',
);
// Exclude some pages from Audience Extension.
$conf['drupalorg_perfect_audience_excluded_pages'] = 'user
user/login
user/register
user/password
project/issues*';
// MailChimp list desicriptions.
$conf['drupalorg_mailchimp_list_descriptions'] = array(
  'Drupal Weekly Newsletter (coming soon)' => 'Weekly updates on all things Drupal.',
  'Drupal Weekly Newsletter' => 'Weekly updates on all things Drupal.',
  'Drupal Association News' => 'Twice monthly news from the Drupal Association.',
  'DrupalCon News' => 'News about DrupalCon locations, deadlines, and more.',
  'Special Offers from Drupal Supporters (coming soon)' => 'Deals on hosting, products and services, sent by the Drupal Association twice per month.',
  'Special Offers from Drupal Supporters' => 'Deals on hosting, products and services, sent by the Drupal Association twice per month.',
  'Information for Drupal Service Providers' => 'Monthly updates for companies who provide Drupal services.',
  'Educational Opportunities from the Drupal Association' => 'Monthly news about webcasts, training, Global Training Days, sprints, and more.',
);

// Version control.
$conf['versioncontrol_repository_plugin_defaults_git_author_mapper'] = 'drupalorg_mapper';
$conf['versioncontrol_repository_plugin_defaults_git_committer_mapper'] = 'drupalorg_mapper';
$conf['versioncontrol_repository_plugin_defaults_git_user_mapping_methods'] = 'drupalorg_mapper';
$conf['versioncontrol_repository_plugin_defaults_git_repomgr'] = 'drupalorg_repomgr';
$conf['versioncontrol_repository_plugin_defaults_git_auth_handler'] = 'account';
$conf['versioncontrol_repository_plugin_defaults_git_webviewer_url_handler'] = 'drupalorg_gitweb_rewrite';
$conf['versioncontrol_repository_synchronizer_options_git_default'] = array(
  // Only map branch/commit relations.
  'operation_labels_mapping' => 0x01,
);

$conf['versioncontrol_repository_plugin_defaults_git_event_processors'] = array(
  'git_commits_as_comment',
);
$conf['versioncontrol_repository_plugin_defaults_git_event_processors_data'] = array(
  'git_commits_as_comment' => array(
    'message_pattern' => '/#(\\d+)\\b/',
  ),
);

// Allow local images from domains that have served Drupal.org. See
// https://drupal.org/node/1737022 & https://drupal.org/node/2257291
$conf['filter_html_image_secure_domains'] = array(
  'drupal.org',
  'www.drupal.org',
);

// Do not let metatag run amok. See https://drupal.org/node/2233753.
$conf['metatag_load_all_pages'] = FALSE;

// Prevent https://drupal.org/node/2284483 from happening again.
$conf['pathauto_node_project_core_pattern'] = 'project/[node:field_project_machine_name]';
$conf['pathauto_node_project_core_pattern_sandbox'] = 'sandbox/[node:author:git-username]/[node:field_project_machine_name]';
$conf['pathauto_node_project_distribution_pattern'] = 'project/[node:field_project_machine_name]';
$conf['pathauto_node_project_distribution_pattern_sandbox'] = 'sandbox/[node:author:git-username]/[node:field_project_machine_name]';
$conf['pathauto_node_project_drupalorg_pattern'] = 'project/[node:field_project_machine_name]';
$conf['pathauto_node_project_drupalorg_pattern_sandbox'] = 'sandbox/[node:author:git-username]/[node:field_project_machine_name]';
$conf['pathauto_node_project_module_pattern'] = 'project/[node:field_project_machine_name]';
$conf['pathauto_node_project_module_pattern_sandbox'] = 'sandbox/[node:author:git-username]/[node:field_project_machine_name]';
$conf['pathauto_node_project_theme_engine_pattern'] = 'project/[node:field_project_machine_name]';
$conf['pathauto_node_project_theme_engine_pattern_sandbox'] = 'sandbox/[node:author:git-username]/[node:field_project_machine_name]';
$conf['pathauto_node_project_theme_pattern'] = 'project/[node:field_project_machine_name]';
$conf['pathauto_node_project_theme_pattern_sandbox'] = 'sandbox/[node:author:git-username]/[node:field_project_machine_name]';
$conf['pathauto_node_project_translation_pattern'] = 'project/[node:field_project_machine_name]';
$conf['pathauto_node_project_translation_pattern_sandbox'] = 'sandbox/[node:author:git-username]/[node:field_project_machine_name]';

// Allow all TLDs in Link fields. List from
// https://www.icann.org/resources/pages/tlds-2012-02-25-en
// Version 2014102800, Last Updated Tue Oct 28 07:07:01 2014 UTC
// Omitting TLDs covered by LINK_DOMAINS and 2-character TLDs.
$conf['link_extra_domains'] = array(
  'abogado', 'academy', 'accountants', 'active', 'actor', 'agency', 'airforce',
  'allfinanz', 'alsace', 'archi', 'army', 'associates', 'attorney', 'auction',
  'audio', 'autos', 'axa', 'band', 'bar', 'bargains', 'bayern', 'beer',
  'berlin', 'best', 'bid', 'bike', 'bio', 'black', 'blackfriday', 'blue',
  'bmw', 'bnpparibas', 'boo', 'boutique', 'brussels', 'budapest', 'build',
  'builders', 'business', 'buzz', 'bzh', 'cab', 'cal', 'camera', 'camp',
  'cancerresearch', 'capetown', 'capital', 'caravan', 'cards', 'care',
  'career', 'careers', 'casa', 'cash', 'catering', 'center', 'ceo', 'cern',
  'channel', 'cheap', 'christmas', 'chrome', 'church', 'citic', 'city',
  'claims', 'cleaning', 'click', 'clinic', 'clothing', 'club', 'codes',
  'coffee', 'college', 'cologne', 'community', 'company', 'computer', 'condos',
  'construction', 'consulting', 'contractors', 'cooking', 'cool', 'country',
  'credit', 'creditcard', 'crs', 'cruises', 'cuisinella', 'cymru', 'dad',
  'dance', 'dating', 'day', 'deals', 'degree', 'democrat', 'dental', 'dentist',
  'desi', 'diamonds', 'diet', 'digital', 'direct', 'directory', 'discount',
  'dnp', 'domains', 'durban', 'dvag', 'eat', 'education', 'email', 'emerck',
  'engineer', 'engineering', 'enterprises', 'equipment', 'esq', 'estate',
  'eus', 'events', 'exchange', 'expert', 'exposed', 'fail', 'farm', 'feedback',
  'finance', 'financial', 'fish', 'fishing', 'fitness', 'flights', 'florist',
  'flsmidth', 'fly', 'foo', 'forsale', 'foundation', 'frl', 'frogans', 'fund',
  'furniture', 'futbol', 'gal', 'gallery', 'gbiz', 'gent', 'gift', 'gifts',
  'gives', 'glass', 'gle', 'global', 'globo', 'gmail', 'gmo', 'gmx', 'google',
  'gop', 'graphics', 'gratis', 'green', 'gripe', 'guide', 'guitars', 'guru',
  'hamburg', 'haus', 'healthcare', 'help', 'here', 'hiphop', 'hiv', 'holdings',
  'holiday', 'homes', 'horse', 'host', 'hosting', 'house', 'how', 'ibm',
  'immo', 'immobilien', 'industries', 'ing', 'ink', 'institute', 'insure',
  'international', 'investments', 'jetzt', 'joburg', 'juegos', 'kaufen', 'kim',
  'kitchen', 'kiwi', 'koeln', 'krd', 'kred', 'lacaixa', 'land', 'lawyer',
  'lease', 'lgbt', 'life', 'lighting', 'limited', 'limo', 'link', 'loans',
  'london', 'lotto', 'ltda', 'luxe', 'luxury', 'maison', 'management', 'mango',
  'market', 'marketing', 'media', 'meet', 'melbourne', 'meme', 'menu', 'miami',
  'mini', 'moda', 'moe', 'monash', 'mortgage', 'moscow', 'motorcycles', 'mov',
  'nagoya', 'navy', 'network', 'neustar', 'new', 'nexus', 'ngo', 'nhk',
  'ninja', 'nra', 'nrw', 'nyc', 'okinawa', 'ong', 'onl', 'ooo', 'organic',
  'otsuka', 'ovh', 'paris', 'partners', 'parts', 'pharmacy', 'photo',
  'photography', 'photos', 'physio', 'pics', 'pictures', 'pink', 'pizza',
  'place', 'plumbing', 'pohl', 'poker', 'post', 'praxi', 'press', 'prod',
  'productions', 'prof', 'properties', 'property', 'pub', 'qpon', 'quebec',
  'realtor', 'recipes', 'red', 'rehab', 'reise', 'reisen', 'ren', 'rentals',
  'repair', 'report', 'republican', 'rest', 'restaurant', 'reviews', 'rich',
  'rio', 'rip', 'rocks', 'rodeo', 'rsvp', 'ruhr', 'ryukyu', 'saarland', 'sarl',
  'sca', 'scb', 'schmidt', 'schule', 'scot', 'services', 'sexy', 'shiksha',
  'shoes', 'singles', 'social', 'software', 'sohu', 'solar', 'solutions',
  'soy', 'space', 'spiegel', 'supplies', 'supply', 'support', 'surf',
  'surgery', 'suzuki', 'systems', 'taipei', 'tatar', 'tattoo', 'tax',
  'technology', 'tel', 'tienda', 'tips', 'tirol', 'today', 'tokyo', 'tools',
  'top', 'town', 'toys', 'trade', 'training', 'tui', 'university', 'uno',
  'uol', 'vacations', 'vegas', 'ventures', 'versicherung', 'vet', 'viajes',
  'villas', 'vision', 'vlaanderen', 'vodka', 'vote', 'voting', 'voto',
  'voyage', 'wales', 'wang', 'watch', 'webcam', 'website', 'wed', 'wedding',
  'whoswho', 'wien', 'wiki', 'williamhill', 'wme', 'work', 'works', 'world',
  'wtc', 'wtf', 'xn--1qqw23a', 'xn--3bst00m', 'xn--3ds443g', 'xn--3e0b707e',
  'xn--45brj9c', 'xn--4gbrim', 'xn--55qw42g', 'xn--55qx5d', 'xn--6frz82g',
  'xn--6qq986b3xl', 'xn--80adxhks', 'xn--80ao21a', 'xn--80asehdb',
  'xn--80aswg', 'xn--90a3ac', 'xn--c1avg', 'xn--cg4bki',
  'xn--clchc0ea0b2g2a9gcd', 'xn--czr694b', 'xn--czru2d', 'xn--d1acj3b',
  'xn--d1alf', 'xn--fiq228c5hs', 'xn--fiq64b', 'xn--fiqs8s', 'xn--fiqz9s',
  'xn--fpcrj9c3d', 'xn--fzc2c9e2c', 'xn--gecrj9c', 'xn--h2brj9c',
  'xn--i1b6b1a6a2e', 'xn--io0a7i', 'xn--j1amh', 'xn--j6w193g', 'xn--kprw13d',
  'xn--kpry57d', 'xn--kput3i', 'xn--l1acc', 'xn--lgbbat1ad8j', 'xn--mgb9awbf',
  'xn--mgba3a4f16a', 'xn--mgbaam7a8h', 'xn--mgbab2bd', 'xn--mgbayh7gpa',
  'xn--mgbbh1a71e', 'xn--mgbc0a9azcg', 'xn--mgberp4a5d4ar', 'xn--mgbx4cd0ab',
  'xn--ngbc5azd', 'xn--node', 'xn--nqv7f', 'xn--nqv7fs00ema', 'xn--o3cw4h',
  'xn--ogbpf8fl', 'xn--p1acf', 'xn--p1ai', 'xn--pgbs0dh', 'xn--q9jyb4c',
  'xn--rhqv96g', 'xn--s9brj9c', 'xn--ses554g', 'xn--unup4y',
  'xn--vermgensberater-ctb', 'xn--vermgensberatung-pwb', 'xn--vhquv',
  'xn--wgbh1c', 'xn--wgbl6a', 'xn--xhq521b', 'xn--xkc2al3hye2a',
  'xn--xkc2dl3a5ee0h', 'xn--yfro4i67o', 'xn--ygbi2ammx', 'xn--zfr164b', 'xxx',
  'xyz', 'yachts', 'yandex', 'yoga', 'yokohama', 'youtube', 'zip', 'zone',
);

// Login using email address.
$conf['logintoboggan_login_with_email'] = TRUE;
// Use two e-mail fields on registration form.
$conf['logintoboggan_confirm_email_at_registration'] = TRUE;
// Set password.
$conf['logintoboggan_user_email_verification'] = FALSE;
// Automatically login.
$conf['logintoboggan_immediate_login_on_register'] = TRUE;
// Present a unified login/registration page.
$conf['logintoboggan_unified_login'] = FALSE;

/**
 * CLI
 */
if (php_sapi_name() === 'cli') {
  ini_set('memory_limit', '800M');
}

/**
 * todo Variable overrides from D6. Exctract and comment when used. Delete when
 * confirmed unused.
 */
/*
$conf = array(
  // Solr
  'apachesolr_cron_limit' => 1000,
  'apachesolr_optimize_interval' => 0,
  'search_cron_limit' => 1000,

  // Project
  'project_issue_invalid_releases' => array(94702),
  'project_issue_global_subscribe_page' => FALSE,
  'project_usage_show_weeks' => 4,
  'project_browse_nodes' => 200,
  'project_issue_hook_cron' => FALSE,
);
*/

// Memcache configuration.
$conf['memcache_key_prefix'] = 'www.drupal.org';

/**
 * Include a common settings file if it exists.
 */
$common_settings = '/var/www/settings.common.php';
if (file_exists($common_settings)) {
  include $common_settings;
}
/**
 * Codifying the exclusions of specific projects for qa.drupal.org
 */
$conf['project_dependency_excluded_dependencies'] = array(
  'b2b_store_solution',
  'category',
  'contact',
  'contextual',
  'corporative_site',
  'drupal',
  'file',
  'leftandright',
  'onepagecv',
  'opendeals',
  'quizz', // Includes a copy of quiz.
  'rebuild', // Includes a copy of Drupal core.
  'rulesmonkey',
  'simpletest',
  'starterkit_touchpro',
  'testbot_exercise',
  'translation',
  'uberpaltw',
  'webform_patched',
);

/**
 * Include a local settings file if it exists.
 */
$local_settings = dirname(__FILE__) . '/settings.local.php';
if (file_exists($local_settings)) {
  include $local_settings;
}
