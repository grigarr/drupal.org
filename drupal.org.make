core = 7.x
api = 2
projects[drupal][version] = "7.35"


; Core hacks :(

; Race condition in node_save() when not using DB for cache_field
; https://www.drupal.org/node/1679344#comment-9027781
projects[drupal][patch][] = "https://www.drupal.org/files/issues/drupal-7.28-transactional-cache_0.patch"

; Forums performance improvements
; https://www.drupal.org/node/145353#comment-8008797
projects[drupal][patch][] = "https://www.drupal.org/files/145353.diff"

; Node preview removes file values from node edit form for non-displayed items
; https://www.drupal.org/node/2001308#comment-7516521
projects[drupal][patch][] = "https://www.drupal.org/files/node-preview-2001308-1.patch"

; Calling node_view for the same node with multiple view modes on the same page
; does not correctly attach fields
; https://drupal.org/node/1289336#comment-7981043
projects[drupal][patch][] = "https://www.drupal.org/files/issues/1289336-reroll_of_25-drupal.org-do-not-test.patch"


; Modules

; advagg 2.8 is upcoming
; https://www.drupal.org/node/2400583
projects[advagg][version] = "2.x-dev"
projects[advagg][download][revision] = "4477985"

projects[apachesolr][version] = "1.5"
projects[apachesolr_multisitesearch][version] = "1.x-dev"
projects[apachesolr_multisitesearch][download][revision] = "f64671a"
projects[beanstalkd][version] = "1.0-rc3"
projects[bueditor][version] = "1.8"
projects[cache_consistent][version] = "1.2"
projects[codefilter][version] = "1.x-dev"
projects[codefilter][download][revision] = "1481afb"
projects[comment_fragment][version] = "1.1"
projects[conflict][version] = "1.0"
projects[ctools][version] = "1.7"
projects[d3][version] = "1.x-dev"
projects[d3][download][revision] = "56b94df"
projects[d3_sparkline][version] = "1.x-dev"
projects[d3_sparkline][download][revision] = "2300b32"
projects[dereference_list][version] = "1.x-dev"
projects[dereference_list][download][revision] = "4acf25d"
projects[diff][version] = "3.2"
projects[entity][version] = "1.6"
projects[entity_modified][version] = "1.2"
projects[entityreference][version] = "1.1"
projects[extended_file_field][version] = "1.x-dev"
projects[extended_file_field][download][revision] = "b1ca3cf"
projects[facetapi][version] = "1.3"
projects[fasttoggle][version] = "1.6"
projects[features][version] = "2.3"
projects[field_collection][version] = "1.x-dev"
projects[field_collection][download][revision] = "edb4e7d"
projects[field_extrawidgets][version] = "1.1"
projects[field_formatter_settings][version] = "1.1"
projects[field_group][version] = "1.3"
projects[filter_html_image_secure][version] = "1.x-dev"
projects[filter_html_image_secure][download][revision] = "aa1f8e5"
projects[flag][version] = "2.1"
projects[flag_tracker][version] = "1.x-dev"
projects[flag_tracker][download][revision] = "47d3aac"
projects[flot][version] = "1.0"
projects[geolocation][version] = "1.4"

; Errors [notices] on 404 pages, waiting for 7.x-2.5 release.
; https://www.drupal.org/node/2126561
projects[google_admanager][version] = "2.x-dev"
projects[google_admanager][download][revision] = "d38afeb"

projects[google_analytics][version] = "1.4"
projects[homebox][version] = "2.0-rc1"
projects[honeypot][version] = "1.15"
projects[httpbl][version] = "1.0"
projects[jcarousel][version] = "2.6"
projects[libraries][version] = "2.1"
projects[link][version] = "1.1"

projects[logintoboggan][version] = "1.4"
; Various one-time-login and validation links don't work with Drupal 6.35 and Drupal 7.35
; https://www.drupal.org/node/2455049#comment-9734957
projects[logintoboggan][patch][] = "https://www.drupal.org/files/issues/logintoboggan-drupal-7.35-compatibility-2455049-1.patch"

projects[machine_name][version] = "1.x-dev"
projects[machine_name][download][revision] = "94b7446"

; dev needed for https://www.drupal.org/node/2323853, Changing emailaddress results in multiple list subscriptions
projects[mailchimp][version] = "3.x-dev"
projects[mailchimp][download][revision] = "2c98def"

projects[memcache][version] = "1.3-rc1"
projects[metatag][version] = "1.x-dev"
projects[metatag][download][revision] = "ede93e3"
projects[migrate][version] = "2.5"
projects[mollom][version] = "2.13"
projects[multiple_email][version] = "1.0-beta4"
projects[nodechanges][version] = "1.x-dev"
projects[nodechanges][download][revision] = "793b181"
projects[paranoia][version] = "1.3"
projects[pathauto][version] = "1.2"
projects[project][version] = "2.x-dev"
projects[project][download][revision] = "97d4cad"
projects[project_dependency][version] = "1.0-beta3"
projects[project_git_instructions][version] = "1.x-dev"
projects[project_git_instructions][download][revision] = "8eb1346"
projects[project_issue][version] = "2.x-dev"
projects[project_issue][download][revision] = "10ca715"
projects[project_issue_file_test][version] = "2.x-dev"
projects[project_issue_file_test][download][revision] = "6c30400"
projects[project_solr][version] = "1.x-dev"
projects[project_solr][download][revision] = "712305d"
projects[r4032login][version] = "1.8"
projects[redirect][version] = "1.0-rc1"

; Fix and prevent circular redirects
; https://www.drupal.org/node/1796596#comment-8506117
projects[redirect][patch][] = "https://www.drupal.org/files/issues/redirect.circular-loops.1796596-146.patch"

projects[references][version] = "2.1"
projects[render_cache][version] = "1.0"
projects[restws][version] = "2.3"
projects[role_activity][version] = "3.0-alpha2"
projects[sampler][version] = "1.0"
projects[search_api][version] = "1.x-dev"
projects[search_api][download][revision] = "fcb6474"
projects[search_api_db][version] = "1.x-dev"
projects[search_api_db][download][revision] = "5c9124d"

; Do not re-tokenize text
; https://www.drupal.org/node/2201041#comment-9174517
projects[search_api_db][patch][] = "https://www.drupal.org/files/issues/2201041-drupal.org-do-not-test.patch"

projects[security_review][version] = "1.1"
projects[sshkey][version] = "2.0"
projects[symfony][version] = "2.0-alpha4"
projects[token][version] = "1.5"
projects[token_formatters][version] = "1.2"
projects[url][version] = "1.0"
projects[user_restrictions][version] = "1.0"
projects[varnish][version] = "1.0-beta2"
projects[versioncontrol][version] = "1.x-dev"
projects[versioncontrol][download][revision] = "d6df95b"
projects[versioncontrol_git][version] = "1.x-dev"
projects[versioncontrol_git][download][revision] = "a280f89"
projects[versioncontrol_project][version] = "1.x-dev"
projects[versioncontrol_project][download][revision] = "38f5074"
projects[views][version] = "3.10"
projects[views_bulk_operations][version] = "3.1"
projects[views_content_cache][version] = "3.0-alpha3"
projects[views_field_view][version] = "1.1"
projects[views_litepager][version] = "3.0"
projects[waiting_queue][version] = "1.2"


; Custom modules
projects[drupalorg][version] = "3.x-dev"
projects[drupalorg][download][revision] = "11f49b5"


; Libraries

libraries[d3][destination] = "libraries"
libraries[d3][directory_name] = "d3"
libraries[d3][download][type] = "get"
libraries[d3][download][url] = "https://github.com/mbostock/d3/releases/download/v3.4.4/d3.zip"

libraries[pheanstalk][destination] = "libraries"
libraries[pheanstalk][directory_name] = "pheanstalk"
libraries[pheanstalk][download][type] = "git"
libraries[pheanstalk][download][url] = "https://github.com/pda/pheanstalk.git"
libraries[pheanstalk][download][revision] = "f5ea533"

libraries[flot][destination] = "modules"
libraries[flot][directory_name] = "flot/flot"
libraries[flot][download][type] = "get"
libraries[flot][download][url] = "http://www.flotcharts.org/downloads/flot-0.7.zip"

libraries[mailchimp][destination] = "libraries"
libraries[mailchimp][directory_name] = "mailchimp"
libraries[mailchimp][download][type] = "get"
libraries[mailchimp][download][url] = "https://bitbucket.org/mailchimp/mailchimp-api-php/get/2.0.6.tar.bz2"


; Common for Drupal.org D7 sites.
includes[drupalorg_common] = "https://bitbucket.org/drupalorg-infrastructure/drupal.org-sites-common/raw/7.x/drupal.org-common.make"

; Responsive theme!
projects[bluecheese][download][branch] = "branded-2.x"
